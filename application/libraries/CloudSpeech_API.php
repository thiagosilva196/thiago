<?php


# Includes the autoloader for libraries installed with composer
require 'vendor/autoload.php';

# Imports the Google Cloud client library
use Google\Cloud\Speech\V1\SpeechClient;
use Google\Cloud\Speech\V1\RecognitionAudio;
use Google\Cloud\Speech\V1\RecognitionConfig;
use Google\Cloud\Speech\V1\RecognitionConfig\AudioEncoding;

class CloudSpeech_API {

    public function transcript($audio, $lang){

        putenv('GOOGLE_APPLICATION_CREDENTIALS=C:\xampp\htdocs\lp2\at02\thiago\LP2 - Consumo de APIs-dd7587d43bbc.json');

        # The name of the audio file to transcribe
        $audioFile = './assets/audio/'.$audio;

        # get contents of a file into a string
        $content = file_get_contents($audioFile);

        # set string as audio content
        $audio = (new RecognitionAudio())
            ->setContent($content);

        # The audio file's encoding, sample rate and language
        $config = new RecognitionConfig([
            //'encoding' => AudioEncoding::LINEAR16,
            //'sample_rate_hertz' => 32000,
            'language_code' => $lang
        ]);

        # Instantiates a client
        $client = new SpeechClient();

        # Detects speech in the audio file
        $response = $client->recognize($config, $audio);

        # Print most likely transcription
       
        $i = 0;
        foreach ($response->getResults() as $result) {
            $alternatives = $result->getAlternatives();
            $mostLikely = $alternatives[0];
            $transcript[$i] = $mostLikely->getTranscript();
            //printf('Transcript: %s' . PHP_EOL, $transcript);
            $i++;
            
        }
        $string = implode("", $transcript);
        //printf('string; %s'. PHP_EOL, $string);
        return $string;
            }

}