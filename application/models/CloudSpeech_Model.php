<?php 

include_once APPPATH.'libraries/CloudSpeech_API.php';

class CloudSpeech_Model extends CI_Model{

public function transcricao($audio, $lang){
    $api = new CloudSpeech_API();
    $transcript = $api->transcript($audio, $lang);
    return $transcript;
    }
    
public function upload(){
    $language = $this->input->post('lang');
		$config['upload_path'] = './assets/audio';
        $config['allowed_types'] = 'wav|flac';
        $this->load->library('upload', $config);

			if(!$this->upload->do_upload('audio')){
            $error = array('error' => $this->upload->display_errors());
		}
		else{
            
            $file_data = $this->upload->data();
            $data = array("arquivo" => $file_data['file_name'], "linguagem" => $language);
            $this->db->insert('audio', $data);
            echo '<script>alert("Upload realizado com sucesso!")</script>';
            redirect('cloudspeech/transcricao/'.$file_data['file_name'].'/'.$language);
		}
    }

    public function relatorio(){
        $sql = "SELECT * FROM audio";
        $relatorio = $this->db->query($sql);
        $data = $relatorio->result();
        return $data;
    }
}