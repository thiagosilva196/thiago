<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class CloudSpeech extends CI_Controller {

	public function index()
	{
		$this->load->view('common/header');
		$this->load->view('common/navbar');
		$this->load->view('card_identificacao');
		$this->load->view('common/footer');
	}

	public function api()
	{
			$this->load->view('common/header');
			$this->load->view('common/navbar');
			$this->load->view('api_demo');
			$this->load->view('cloud_speech');
			$this->load->model('CloudSpeech_Model', 'API');
			$this->API->upload();
			$this->load->view('form_api');
			$this->load->view('common/footer');
		}

		public function transcricao($audio, $lang)
	{
			$this->load->view('common/header');
			$this->load->view('common/navbar');
			$this->load->view('api_demo');
			$this->load->view('cloud_speech');
			$this->load->model('CloudSpeech_Model', 'API');
			$dados['transcript'] = $this->API->transcricao($audio, $lang);
			$this->load->view('transcricao', $dados);
			$this->load->view('common/footer');
		}

		public function sobre_api(){
			$this->load->view('common/header');
			$this->load->view('common/navbar');
			$this->load->view('sobre');
			$this->load->view('common/footer');
		}

		public function relatorio(){
			$this->load->view('common/header');
			$this->load->view('common/navbar');
			$this->load->model('CloudSpeech_Model', 'API');
			$dados['relatorio'] = $this->API->relatorio();
			$this->load->view('relatorio', $dados);
			$this->load->view('common/footer');
		}
		
    
    
}
