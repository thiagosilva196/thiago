  <!-- Full Page Intro -->
  <div class="view" style="background-image: url('https://mdbootstrap.com/img/Photos/Others/images/89.jpg'); background-repeat: no-repeat; background-size: cover; background-position: center center;">
    <!-- Mask & flexbox options-->
    <div class="mask rgba-indigo-strong d-flex justify-content-center align-items-center">
      <!-- Content -->
      <div class="container text-white" style="position: absolute; top: 13%;">
        <h1 class="text-center"><b>Sobre a API</b></h1>
        <br />
        <!--1. Quais são as principais características da API e suas finalidades? -->
        <h4>A API Google Cloud Speech tem como principal característica o reconhecimento de fala, com 
        a finalidade de converter o áudio em texto.<h4>
        <br />
        <!--2. O quê é possível criar com o uso desta API?  -->
        <h4>É possível criar por exemplo, um sistema de navegação por comandos de voz, ou ainda possibilitar
        funções de acessibilidade para deficientes auditivos.</h4>
        <br />
        <!--3. Quais são as restrições para o uso da API? Tem custo? Tem boa documentação? -->
        <h4>Para usar a API, é necessário que seja gerada uma chave privada (key), que é única para cada projeto.
        Esta chave pode ser gerada sem nenhum custo. Por ter sido desenvolvida pela Google, esta API possui
        uma documentação bastante abrangente e completa, contendo todas as informações necessárias para
        a correta utilização.</h4>
        <br />
        <!--4. Quais foram os passos necessários para a implementação da sua aplicação com o uso desta API? -->
        <h4>É necessário gerar uma key no Google Cloud e também instalar as dependências da API via Composer.
        Após feito isso basta criar uma library com o código fornecido na documentação oficial. A API suporta arquivos nos 
        formatos Raw e Flac e também é possível capturar a fala diretamente de um microfone, porém essa última 
        funcionalidade não foi implementada neste projeto.</h4>
        <br />
      </div>
          <!--Grid column-->
        </div>
        <!--Grid row-->
      </div>
      <!-- Content -->
      </div>
    <!-- Mask & flexbox options-->
  </div>
  <!-- Full Page Intro -->
</header>
<!-- Main navigation -->