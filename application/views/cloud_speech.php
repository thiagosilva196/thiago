<!--Grid row-->
<div class="row pt-lg-5 mt-lg-5">
          <!--Grid column-->
          <div class="col-md-6 mb-5 mt-md-0 mt-5 white-text text-center text-md-left wow fadeInLeft"
            data-wow-delay="0.3s">
            <h1 class="display-4 font-weight-bold">Google Cloud Speech</h1>
            <hr class="hr-light">
            <h6 class="mb-3">O Cloud Speech-to-Text permite a fácil integração das tecnologias 
            de reconhecimento de fala do Google nos aplicativos do desenvolvedor. Envie áudio 
            e receba uma transcrição de texto do serviço da Speech-to-Text API.</h6>
          </div>
          <!--Grid column-->