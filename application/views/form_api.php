         <!--Grid column-->
         <div class="col-md-6 col-xl-5 mb-4">
            <!--Form-->
            <div class="card wow fadeInRight" data-wow-delay="0.3s">
              <div class="card-body z-depth-2">
                    <form class="text-center p-5" method="POST" enctype="multipart/form-data">
                        <p class="h4 mb-4">Selecione um arquivo de áudio</p>
                            <input type="file" id="audio" name="audio" class="form-control mb-4" required>
                            <select class="browser-default custom-select mb-4" name="lang" required>
                          <option value="" disabled selected>Selecione um idioma...</option>
                          <option value="pt-BR">Português-BR</option>
                          <option value="en-US">Inglês-US</option>
                        </select>
                        <button class="btn btn-info btn-block" type="submit">Enviar</button>
                    </form>
              </div>
            </div>
            <!--/.Form-->
          </div>
          <!--Grid column-->
        </div>
        <!--Grid row-->
      </div>
      <!-- Content -->
      </div>
    <!-- Mask & flexbox options-->
  </div>
  <!-- Full Page Intro -->