  <!-- Full Page Intro -->
  <div class="view" style="background-image: url('https://mdbootstrap.com/img/Photos/Others/images/89.jpg'); background-repeat: no-repeat; background-size: cover; background-position: center center;">
    <!-- Mask & flexbox options-->
    <div class="mask rgba-indigo-strong d-flex justify-content-center align-items-center">
      <!-- Content -->
      <div class="container text-center" style="position: fixed; top: 15%;">
        <h1 class="text-white"><b>Relatório de Interações</b></h1>
        <br />
        <table class="table table-dark">
          <thead>
              <tr>
                  <th scope="col">ID</th>
                  <th scope="col">Arquivo</th>
                  <th scope="col">Linguagem</th>
                  <th scope="col">Última modificação</th>
                  <th scope="col"></th>
              </tr>
          </thead>
          <tbody>
              <?php foreach ($relatorio as $resultado): ?>
              <a href="#">
                  <tr>
                      <th scope="row"><?= $resultado->id ?></th>
                      <td><?= $resultado->arquivo ?></td>
                      <td><?= $resultado->linguagem?></td>
                      <td><?= $resultado->ultima_modificacao ?></td>
                      <!--<td><a href="<?= base_url('Api/visualizar/'.$resultado->audio.'/'.$resultado->language.'') ?>">
                      <i class="fas fa-eye"></i></a> -->
                      </td>
                  </tr>
                  <?php endforeach; ?>
          </tbody>
      </table>
      <!-- Content -->
      </div>
    <!-- Mask & flexbox options-->
  </div>
  <!-- Full Page Intro -->
</header>
<!-- Main navigation -->